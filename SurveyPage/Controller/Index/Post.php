<?php

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Context;
 
class Post extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $data = $this->getRequest()->getPostValue();
        $err = "";
        $err .= isset($data["email"]) && filter_var($data["email"], FILTER_VALIDATE_EMAIL)? "" : "Sähköposti osoite ei ole validi|";
        $err .= isset($data["sender"]) && strlen($data["sender"])>2? "" : "Nimi on liian lyhyt|"; 
        $err .= isset($data["answer"])? "" : "Et valinnut parasta tuotetta|";
        if($err != ""){
           header('Location: /survey/index/index?err='.$err);
        } else {
             $objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
            $post = $objectManager->create('Survey\SurveyPage\Model\Post');
            $post->setRating($data["rating"]);
            $post->setSender($data["sender"]);
            $post->setAnswer(isset($data["answer"]) ? $data["answer"] : "");
            $post->save();
        }
   
        return $resultPage;
    }
  
}