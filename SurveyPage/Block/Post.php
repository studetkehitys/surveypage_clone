<?php
namespace Survey\SurveyPage\Block;

class Post extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function GetAnswers(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
        $post = $objectManager->create('Survey\SurveyPage\Model\ResourceModel\Post\Collection');
        //$post->addAttributeToSelect("*");
		return $post->load();
	}

}